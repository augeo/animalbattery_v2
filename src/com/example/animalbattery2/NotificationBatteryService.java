package com.example.animalbattery2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.BatteryManager;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

public class NotificationBatteryService extends Service {
	
	public static Bitmap Not_bmp;
	
	private NotificationManager _notifManager;
	private Notification _notification;
	
	private static int _lastBatteryLevel = 0;
	private int iconData = 1;

	private static int _currentLevel = -1;
	
	BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			//
			String _status = "";
			//
			
			// Get Battery Level,Battery Scale,Battery Temperature  From Battery Manager by intent
			int _batteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
			int _batteryScale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 0);
			int _batteryTemp = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0)/10;
			// Scale Battery Level
			_currentLevel = (_batteryLevel*100)/_batteryScale;
			// Battery Level Changed 
			if(_currentLevel != _lastBatteryLevel){
				_lastBatteryLevel = _currentLevel;
				ShowNotification(iconData,_currentLevel + "% " + _status,_currentLevel,_batteryTemp);
			}
		}
	};
	@Override
    public void onCreate() {
        super.onCreate();
        _lastBatteryLevel = 0;
		// Register receiver that handles screen on and screen off logic
        this.registerReceiver(batteryInfoReceiver,	new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }
	@Override
    public void onDestroy() {
		super.onDestroy();
		// Its will unregister event BroadcastReceiver
		if(batteryInfoReceiver!=null){
			unregisterReceiver(batteryInfoReceiver);
		}
		try{
			//Clear notification if notification posted
			_notifManager.cancel(0);
		}catch(Exception e){
			//Clear notification if notification not post
			ShowNotification(R.drawable.ic_launcher,null,0,0);
			_notifManager.cancel(0);
		}
    }
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@SuppressLint("NewApi")
	// Method post notification
	private void ShowNotification(int _showIcon,String _tickerText,int _batteryLevel,int _batteryTemp){
		// Close old notification before post new notification
		try{
    		_notifManager.cancel(0);
    	}catch(Exception e){}
		
		// Initialize Notification Manager 
		_notifManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		// Initialize Notification
		_notification = new Notification();
		// Set notification items
		_notification.tickerText = null;
		_notification.icon = R.drawable.ic_launcher;
		_notification.when = System.currentTimeMillis();
		_notification.flags = Notification.FLAG_ONGOING_EVENT;
		
		// Intent Notification
		Intent notificationIntent = new Intent(this, MainActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		//Remote View to My Notification Layout
		RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_notification_layout);
		
		// Create variable for total memory
		long total_Ram = GetTotal_RAM_MemorySize();
		// Create variable for available memory
		long available_Ram = GetAvailable_RAM_MemorySize();
		// Create percentage of memory usage
		long percentageMem = (available_Ram*100)/total_Ram;
		
		//Notification icons data
		Bitmap bimap_BateryLevel = null;
		int battery_level = R.drawable.bat_1;
		int battery_temp = R.drawable.tem_1;
		int available_mem = R.drawable.mem_1;
	
		// Temperature Image Changed By Battery Temperature Level
		if(_batteryTemp > 80 && _batteryTemp < 101){		battery_temp = R.drawable.tem_1; } // 80-100
		else if(_batteryTemp > 60 && _batteryTemp < 81){	battery_temp = R.drawable.tem_2; } // 60-80
		else if(_batteryTemp > 40 && _batteryTemp < 61){	battery_temp = R.drawable.tem_3; } // 40-60
		else if(_batteryTemp > 20 && _batteryTemp < 41){	battery_temp = R.drawable.tem_4; } // 20-40
		else {												battery_temp = R.drawable.tem_5; } // 00-20
		
		// Memory Image Changed By RAM Memory Percentage Level
		if(percentageMem > 80 && percentageMem<101){		available_mem = R.drawable.mem_1; } // 80-100
		else if(percentageMem > 60 && percentageMem<81){	available_mem = R.drawable.mem_2; } // 60-80
		else if(percentageMem > 40 && percentageMem<61){	available_mem = R.drawable.mem_3; } // 40-60
		else if(percentageMem > 20 && percentageMem<41){	available_mem = R.drawable.mem_4; } // 20-40
		else{												available_mem = R.drawable.mem_5; } // 00-20
		
		// Battery Iamge Changed By Battery Percentage Level
		if(_currentLevel > 80 && _currentLevel<101){		battery_level = R.drawable.bat_1; } // 80-100
		else if(_currentLevel > 60 && _currentLevel<81){	battery_level = R.drawable.bat_2; } // 60-80
		else if(_currentLevel > 40 && _currentLevel<61){	battery_level = R.drawable.bat_3; } // 40-60
		else if(_currentLevel > 20 && _currentLevel<41){	battery_level = R.drawable.bat_4; } // 20-40
		else{												battery_level = R.drawable.bat_5; } // 00-20
				
		// 
		Bitmap image1 = BitmapFactory.decodeResource(getResources(), battery_level);
		Bitmap image2 = BitmapFactory.decodeResource(getResources(), R.drawable.icondefault);
		//
		Not_bmp = Read_Data_Preference();
		if(Not_bmp != null ){
			image2 = Not_bmp;
		}
		//
		bimap_BateryLevel = ExtenalOperation.OverlayTwoBitmap(image1,image2);
		//
		contentView.setImageViewBitmap(R.id.image1, bimap_BateryLevel);
		contentView.setImageViewResource(R.id.image2, battery_temp);
		contentView.setImageViewResource(R.id.image3, available_mem);
		// Set Notification Text // _batteryLevel
		contentView.setTextViewText(R.id.txv_battery_percentage, _batteryLevel + "%");
		contentView.setTextViewText(R.id.txv_battery_temp, _batteryTemp + "�C");
		contentView.setTextViewText(R.id.txv_available_memory, available_Ram + "MB");
		//
		_notification.contentView = contentView;
		_notification.contentIntent = contentIntent;
		// show notification
		_notifManager.notify(0,_notification);
    }

	// Get 
	private long GetTotal_RAM_MemorySize(){  
	    String str1 = "/proc/meminfo";
	    String str2;        
	    String[] arrayOfString;
	    long initial_memory = 0;
	    try {
		    FileReader localFileReader = new FileReader(str1);
		    BufferedReader localBufferedReader = new BufferedReader(    localFileReader, 8192);
		    str2 = localBufferedReader.readLine();
		    arrayOfString = str2.split("\\s+");
		    for (String num : arrayOfString) {
		    	Log.i(str2, num + "\t");
		    }
		    //total Memory
		    initial_memory = Integer.valueOf(arrayOfString[1]).intValue() * 1024;   
		    localBufferedReader.close();
		    return initial_memory / 1048576L;
	    } 
	    catch (IOException e) 
	    {       
	        return -1;
	    }
	}
	// Get
	private long GetAvailable_RAM_MemorySize(){
		long _available = 0;
		android.app.ActivityManager.MemoryInfo mi = new android.app.ActivityManager.MemoryInfo();
		ActivityManager _activeManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		_activeManager.getMemoryInfo(mi);
		_available = mi.availMem / 1048576L;
		return _available;
	}
	// Get Bitmap file 
	private Bitmap Read_Data_Preference(){
		Bitmap bmp = null;
		String mFilePath = null;
        final SharedPreferences sharedPreferences = getSharedPreferences("myPreferenceImage", Context.MODE_PRIVATE);
        mFilePath = sharedPreferences.getString("file_Imagepath", null);
        Bitmap iconShow_Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.icondefault);
        try{
	        if(mFilePath != null){
	        	iconShow_Bmp = BitmapFactory.decodeFile(mFilePath);   
	        	bmp = ExtenalOperation.MakeImageViewRoundCorner(iconShow_Bmp,(iconShow_Bmp.getWidth()/7));
	        }
        }
        catch(Exception e){
        	bmp = ExtenalOperation.MakeImageViewRoundCorner(iconShow_Bmp,(iconShow_Bmp.getWidth()/7));
        }
        return bmp;
	}
}
