package com.example.animalbattery2;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;

public class ExtenalOperation  {
	// Overlay Combine 2 Bitmap
	public static Bitmap OverlayTwoBitmap(Bitmap bmp1, Bitmap bmp2) {
		
		int bmpWidth = bmp1.getWidth();
		int bmpHeigh = bmp1.getHeight();
		
		int bmpPicWidth = (bmpWidth * 56)/100;
		int bmpPicHeigh = bmpPicWidth;
		
        Bitmap bmOverlay = Bitmap.createBitmap(bmpWidth, bmpHeigh, bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(Bitmap.createScaledBitmap(MakeImageViewRoundCorner(bmp2, bmpPicWidth/7 ) , bmpPicWidth, bmpPicHeigh,true), ((bmpWidth * 42)/100), ((bmpHeigh/2)-(bmpPicHeigh/2))+5, null);
        
        
        return bmOverlay;
    }
	// Make ImageView Has Round Corner
	public static Bitmap MakeImageViewRoundCorner(Bitmap src, float round) { 
		// Source image size 
		int width = src.getWidth(); 
		int height = src.getHeight(); 
		// create result bitmap output 
		Bitmap result = Bitmap.createBitmap(width, height, Config.ARGB_8888); 
		// set canvas for painting
		Canvas canvas = new Canvas(result); canvas.drawARGB(0, 0, 0, 0); 
		// configure paint 
		final Paint paint = new Paint(); 
		paint.setAntiAlias(true); 
		paint.setColor(Color.BLACK); 
		// configure rectangle for embedding final 
		Rect rect = new Rect(0, 0, width, height); 
		final RectF rectF = new RectF(rect); 
		// draw round rectangle to canvas 
		canvas.drawRoundRect(rectF, round, round, paint); 
		// create Xfer mode 
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN)); 
		// draw source image to canvas 
		canvas.drawBitmap(src, rect, rect, paint); 
		// return round rectangle image 
		return result; 
	}

}
