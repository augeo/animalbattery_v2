package com.example.animalbattery2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.example.animalbattery2.R;

public class MainActivity extends Activity implements OnClickListener {
		
	// Declare variables
	private String[] FilePathStrings;
	private String[] FileNameStrings;
	private File[] listFile;
	
	GridView gridview;
	GridViewAdapter adapter;
	File file;
	
	ImageView imageview;
	String mCurrentPhotoPath;
	
	private static final int PICK_FROM_CAMERA = 1;
	private static final int PICK_FROM_GALLERY = 2;

	private static String fileImagePath = null;
	
	private ImageView imv_BtnStart;
	private ImageView imv_BtnStop;
	private ImageView imv_BtnCamera;
	
	private ImageView imv_framicon;
	private ImageView imv_iconShow;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        
		// Initialize All Controls
		Initialize_All_Controls();
		// Initials 
		Init_FameIcon();
		Init_IconShow();
//		// Set Event OnClick
		Set_Controls_OnClickListener();
		
        Retriev_images();           

        Read_Data_Preference();
	}
        
	private void Read_Data_Preference(){
		String mFilePath = null;
		final SharedPreferences sharedpreferences = getSharedPreferences("myPreferenceImage", Context.MODE_PRIVATE);
		mFilePath = sharedpreferences.getString("file_Imagepath", null);
        Bitmap iconShow_Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.icondefault);
        try{
            if( mFilePath != null){
            	iconShow_Bmp = BitmapFactory.decodeFile(mFilePath);   
            }
        }
        catch(Exception e){
        	iconShow_Bmp = BitmapFactory.decodeResource(getResources(), R.drawable.icondefault);
        }
        imv_iconShow.setImageBitmap(ExtenalOperation.MakeImageViewRoundCorner(iconShow_Bmp,(iconShow_Bmp.getWidth()/7)));
	}
	private void Write_Date_Preference(){
		final SharedPreferences sharedpreferences = getSharedPreferences("myPreferenceImage", Context.MODE_PRIVATE);
		Editor edit = sharedpreferences.edit();
	    edit.putString("file_Imagepath",fileImagePath);
	    edit.commit();
	}
	private void Initialize_All_Controls() {
		// Initialize All Controls
		// Image View as Button
		imv_BtnCamera = (ImageView) findViewById(R.id.imv_BtnCamera);
		imv_BtnStart = (ImageView) findViewById(R.id.imv_BtnStart);
		imv_BtnStop = (ImageView) findViewById(R.id.imv_BtnStop);
		// Image View as Icon
		imv_framicon = (ImageView) findViewById(R.id.imv_IconFrame);
		imv_iconShow = (ImageView) findViewById(R.id.imv_iconshow);
		// Grid View
		gridview = (GridView) findViewById(R.id.gridView1);
	}
	private void Set_Controls_OnClickListener() {
		imv_BtnCamera.setOnClickListener(this);
		imv_BtnStart.setOnClickListener(this);
		imv_BtnStop.setOnClickListener(this);
	}
	
	// Override Method OnClick
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		// Raise imv_BtnStart OnClick Listener
		case R.id.imv_BtnStart:
			BtnStart_OnClick(v);
			break;
		// Raise imv_BtnStop OnClick Listener
		case R.id.imv_BtnStop:
			BtnStop_OnClick(v);
			break;
		// Raise imv_BtnCamera OnClick Listener
		case R.id.imv_BtnCamera:
			BtnCamer_OnClick(v);
			break;
		}
	}
	// Initialize Frame of Icon
	private void Init_FameIcon(){
		Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.iconframe);
		imv_framicon.setImageBitmap(ExtenalOperation.MakeImageViewRoundCorner(bmp,bmp.getWidth()/7));
	}
	// Initialize Icon Show
	private void Init_IconShow(){
		Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.icondefault);
		imv_iconShow.setImageBitmap(ExtenalOperation.MakeImageViewRoundCorner(bmp,bmp.getWidth()/7));
	}
	
//Region Raise Controls Event
	// BtnStart Handle Event
	private void BtnStart_OnClick(View v) {
		// Save File PathName to Shared preference Object
	    Write_Date_Preference();
		// Start com.augeo.runinservice.NotificationBattery Service
        if(!isMyServiceRunning()){
        	startService(new Intent(getBaseContext(),NotificationBatteryService.class));
        }
        else{
        	stopService(new Intent(getBaseContext(),NotificationBatteryService.class));
        	startService(new Intent(getBaseContext(),NotificationBatteryService.class));
        }
	}
	// BtnStop Handle Event
	private void BtnStop_OnClick(View v){
		stopService(new Intent(getBaseContext(),NotificationBatteryService.class));
		Toast.makeText(this, "Service Stopped", Toast.LENGTH_SHORT).show();
	}
	// BtnCamera Handle Event
	private  void BtnCamer_OnClick(View v) {
		// Create New Dialog
		AlertDialog.Builder builder = new AlertDialog.Builder((Context)MainActivity.this);
	    LayoutInflater inflater = ((Activity)(Context)MainActivity.this).getLayoutInflater();
	    builder.setView(inflater.inflate(R.layout.dialog_crop, null));
		final AlertDialog alertDialog = builder.create();
		alertDialog.show();
		// Create and initialize controls in dialog_crop
		final ImageView btn_Gallery = (ImageView) alertDialog.findViewById(R.id.gallery_opt);
		final ImageView btn_Camera = (ImageView) alertDialog.findViewById(R.id.camera_opt);
		final ImageView btn_CloseDialog = (ImageView) alertDialog.findViewById(R.id.btn_d_close);
		
		// Button Close dialog is clicked
        btn_CloseDialog.setOnClickListener(new OnClickListener() {					
			@Override
			public void onClick(View v) {
				alertDialog.dismiss();
			}
		});
		// Button Gallery is clicked
		btn_Gallery.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Create New Intent for 
				Intent intent = new Intent("com.android.camera.action.CROP");
				// call android default gallery
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				//Set crop image information
				SetCropImage(intent);
				// Start Gallery Activity
				startActivityForResult(intent, PICK_FROM_GALLERY);
				// Hide Dialog
				alertDialog.hide();
			}
		});
		// Button Camera is clicked
		btn_Camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// call android default camera
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT,
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString());
				//Set crop image information
				SetCropImage(intent);
				// Start Camera Activity
				startActivityForResult(intent, PICK_FROM_CAMERA);
				// Hide Dialog
				alertDialog.hide();
			}
			
		});
	}

//End Region
	
	
	private boolean isMyServiceRunning() {
		String activityService = MainActivity.ACTIVITY_SERVICE;
	    ActivityManager manager = (ActivityManager) getSystemService(activityService);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (NotificationBatteryService.class.getName().equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}
	
	public void Retriev_images(){
		// Check for SD Card is available or not 
 		if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
 			Toast.makeText(this, "Error! No SDCARD Found!", Toast.LENGTH_LONG).show();
 		} else {
 			// Locate the image folder in your SD Card
 			file = new File(Environment.getExternalStorageDirectory()+ File.separator + "Images");
 			// Create a new folder
 			file.mkdirs();
 		}
 		// Retrieve data 
	    Read_Data_ToGridview();
		// Capture Grid view item click
		gridview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// Create intent of MainActivity
				Intent intent = new Intent(MainActivity.this, MainActivity.class);
				// Pass String arrays FilePathStrings
				intent.putExtra("filepath", FilePathStrings);
				// Pass String arrays FileNameStrings
				intent.putExtra("filename", FileNameStrings);
				// Pass click position
				intent.putExtra("position", position);
				//
				// Get position of items in gridview
				int my_position = intent.getExtras().getInt("position");
				String[] filepath = intent.getStringArrayExtra("filepath");
				// Decode Bitmap from file path
				Bitmap bmp = BitmapFactory.decodeFile(filepath[my_position]);
				// Assign file path of item to variable fileImagePath
				fileImagePath = filepath[my_position];
				// Assign Bitmap to variable in class NotificationBatteryService
				NotificationBatteryService.Not_bmp = bmp;
				// Show Image in ImageView Show
				imv_iconShow.setImageBitmap(ExtenalOperation.MakeImageViewRoundCorner(bmp,bmp.getWidth()/7));
			}

		});		
	}
	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Using try 
		try{
			// If user PICK_FROM_CAMERA for complete Action
			if (requestCode == PICK_FROM_CAMERA) {
				// Variable extras as Bundle for getExtras from Intent
				Bundle extras = data.getExtras();
				if (extras != null) {
					// Variable photo get data from extras.getParcelable
					Bitmap photo = extras.getParcelable("data"); 
					// Assign Bitmap to Imageview imv_iconShow
					imv_iconShow.setImageBitmap(ExtenalOperation.MakeImageViewRoundCorner(photo,photo.getWidth()/7));
					// Assign Bitmap to variable in class NotificationBatteryService
					NotificationBatteryService.Not_bmp = photo;
					// Variable m_extras as Bundle for data.getExtras
					Bundle m_extras = data.getExtras();
					// Get the cropped bitmap
					Bitmap m_thePic = m_extras.getParcelable("data");
					String m_path = Environment.getExternalStorageDirectory().toString();
					File m_imgDirectory = new File(m_path + "/Images/");
					// Check directory is exist, if don't have make new directory
					if (!m_imgDirectory.exists()) m_imgDirectory.mkdir();
					
					OutputStream m_fOut = null;
					File m_file = new File(m_path);
					m_file.delete();
					String m_fileid = System.currentTimeMillis() + "";
					m_file = new File(m_path, "/Images/" + m_fileid + ".png");  
					// Assign file path of item to variable fileImagePath
					fileImagePath = m_file.getPath();
					
					try
					{
						// Check directory is exist, if don't have make new directory
					    if (!m_file.exists()) m_file.createNewFile();
					    
					    m_fOut = new FileOutputStream(m_file);      
					    Bitmap m_bitmap = m_thePic.copy(Bitmap.Config.ARGB_8888, true);
					    m_bitmap.compress(Bitmap.CompressFormat.PNG, 100, m_fOut);
					    m_fOut.flush();
					    m_fOut.close();
					    MediaStore.Images.Media.insertImage(getContentResolver(),
					    		m_file.getAbsolutePath(), m_file.getName(), m_file.getName());

					    // Retrieve data again 
					    Read_Data_ToGridview();
 
					}
					catch (Exception p_e){}
					
					sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,Uri.parse("file://" + Environment.getExternalStorageDirectory())));
				}
			}
			// If user PICK_FROM_GALLERY for complete Action
			if (requestCode == PICK_FROM_GALLERY){
				// Variable extras as Bundle for getExtras from Intent
				Bundle extras = data.getExtras();
				if (extras != null) {
					// Variable extras as Bundle for getExtras from Intent
					Bitmap photo = extras.getParcelable("data");
					// Assign Bitmap to Image View imv_iconShow
					imv_iconShow.setImageBitmap(ExtenalOperation.MakeImageViewRoundCorner(photo,photo.getWidth()/7));
					// Assign Bitmap to variable in class NotificationBatteryService
					NotificationBatteryService.Not_bmp = photo;
					// Variable m_extras as Bundle for data.getExtras
					Bundle m_extras = data.getExtras();
					 // get the cropped bitmap
					Bitmap m_thePic = m_extras.getParcelable("data");
					String m_path = Environment.getExternalStorageDirectory().toString();
					File m_imgDirectory = new File(m_path + "/Images/");
					if (!m_imgDirectory.exists()) m_imgDirectory.mkdir();
					OutputStream m_fOut = null;
					File m_file = new File(m_path);
					m_file.delete();
					String m_fileid = System.currentTimeMillis() + "";
					m_file = new File(m_path, "/Images/" + m_fileid + ".png"); 
					// Assign file path to variable fileImagePath
					fileImagePath = m_file.getPath();
					//
					try
					{
						// Check directory is exist, if don't have make new directory
					    if (!m_file.exists()) m_file.createNewFile();
					    m_fOut = new FileOutputStream(m_file);      
					    Bitmap m_bitmap = m_thePic.copy(Bitmap.Config.ARGB_8888, true);
					    m_bitmap.compress(Bitmap.CompressFormat.PNG, 100, m_fOut);
					    m_fOut.flush();
					    m_fOut.close();
					    MediaStore.Images.Media.insertImage(getContentResolver(),
					            m_file.getAbsolutePath(), m_file.getName(), m_file.getName());

					    // Retrieve data again 
					    Read_Data_ToGridview();
					    
					}
					catch (Exception p_e)
					{
					}
					sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,Uri.parse("file://" + Environment.getExternalStorageDirectory())));
				}
			}
		}catch(Exception e){}
	}
	
	private void Read_Data_ToGridview(){
		// Check Directory
		if (file.isDirectory()) {
			listFile = file.listFiles();
			// Create New String array for FilePath Strings
			FilePathStrings = new String[listFile.length];
			// Create New String array for FileName Strings
			FileNameStrings = new String[listFile.length];
			// For loop get all item in directory
			for (int i = 0; i < listFile.length; i++) {
				// Get the path of the image file
				FilePathStrings[i] = listFile[i].getAbsolutePath();
				// Get the name image file
				FileNameStrings[i] = listFile[i].getName();
			}
		}
		// Pass String arrays to LazyAdapter Class
		adapter = new GridViewAdapter(this, FilePathStrings, FileNameStrings);
		// Set the LazyAdapter to the GridView
		gridview.setAdapter(adapter);
	}
	private void SetCropImage(Intent intent){
		// Set crop image information 
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 200);
		intent.putExtra("aspectY", 200);
		intent.putExtra("outputX", 200);
		intent.putExtra("outputY", 200);
		intent.putExtra("return-data", true);
	}
	
}
