package com.example.animalbattery2;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.animalbattery2.R;

public class GridViewAdapter extends BaseAdapter {
	// Declare variables
	private Activity activity;
	private String[] filepath;
	//private String[] filename;

	private static LayoutInflater inflater = null;

	public GridViewAdapter(Activity a, String[] fpath, String[] fname) {
		activity = a;
		filepath = fpath;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		return filepath.length;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.gridview_item, null);
		
		ImageView image = (ImageView) vi.findViewById(R.id.image);
		Bitmap bmp = BitmapFactory.decodeFile(filepath[position]);
		// Set the decoded bitmap into ImageView
		image.setImageBitmap(ExtenalOperation.MakeImageViewRoundCorner(bmp,bmp.getWidth()/7));
		return vi;
	}
	
	
}
